package com.example.cardealer.service;

import com.example.cardealer.dao.CarRowMapper;
import com.example.cardealer.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CarSearchService {

@Autowired
JdbcTemplate jdbcTemplate;

public boolean validateNotNull(String str){
    return !(str == null || str.trim().length() == 0 || str.length() > 255);
}

public List<Car> search(String str){
    return jdbcTemplate.query("SELECT * FROM cars \n"+
                    "WHERE make LIKE ? OR model LIKE ? \n" +
                    "OR type LIKE ? OR transmission LIKE ? \n"+
                    "OR color LIKE ? OR year LIKE ? \n"+
                    "OR miles LIKE ? OR price LIKE ? \n", new CarRowMapper(),
            new Object[]{"%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%"});
}

}
