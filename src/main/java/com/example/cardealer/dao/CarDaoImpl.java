package com.example.cardealer.dao;

import java.math.BigInteger;

import java.sql.ResultSet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.cardealer.model.Car;

@Repository
public class CarDaoImpl implements CarDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public int save(Car car) {
		return jdbcTemplate.update(
			"INSERT INTO cars (make, model, type, transmission, color, year, miles, price) VALUES(?,?,?,?,?,?,?,?)",
			new Object[] {
				car.getMake(),
				car.getModel(),
				car.getType(),
				car.getTransmission(),
				car.getColor(),
				car.getYear(),
				car.getMiles(),
				car.getPrice(),
			});
    }

	@Override
	public List<Car> findAll() {
		return jdbcTemplate.query("SELECT * FROM cars", new CarRowMapper());
	}

	@Override
	public Car findById(BigInteger id) {
		return jdbcTemplate.queryForObject("SELECT * FROM cars WHERE id=?", new CarRowMapper(), id);
	}
	@Override
	public int update(Car e, BigInteger id) {
		return jdbcTemplate.update("UPDATE cars SET make = ?, model = ?, type = ?, transmission = ?, color = ?, year = ?, miles = ?, price = ? WHERE id = ?",
		new Object[] {
			e.getMake(),
			e.getModel(),
			e.getType(),
			e.getTransmission(),
			e.getColor(),
			e.getYear(),
			e.getMiles(),
			e.getPrice(),
			id
		});
	}

	@Override
    public int deleteById(BigInteger id) {
		return jdbcTemplate.update("DELETE FROM cars WHERE id=?", id);
    }
}
