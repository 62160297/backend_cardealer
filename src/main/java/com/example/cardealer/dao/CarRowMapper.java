package com.example.cardealer.dao;

import com.example.cardealer.model.Car;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarRowMapper implements RowMapper<Car>{
    private final String ID = "id";
    private final String MAKE = "make";
    private final String MODEL = "model";
    private final String TYPE = "type";
    private final String TRANSMISSION = "transmission";
    private final String COLOR = "color";
    private final String YEAR = "year";
    private final String MILES = "miles";
    private final String PRICE = "price";

    public Car mapRow(ResultSet rs, int rowNum) throws SQLException {
        final Car mapperObject = new Car();
        mapperObject.setId(BigInteger.valueOf(rs.getLong(ID)));
        mapperObject.setMake(rs.getString(MAKE));
        mapperObject.setModel(rs.getString(MODEL));
        mapperObject.setType(rs.getString(TYPE));
        mapperObject.setTransmission(rs.getString(TRANSMISSION));
        mapperObject.setColor(rs.getString(COLOR));
        mapperObject.setYear(rs.getString(YEAR));
        mapperObject.setMiles(rs.getFloat(MILES));
        mapperObject.setPrice(rs.getFloat(PRICE));

        return mapperObject;

    };
}
